from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging

from django.core import management

from django.conf import settings
from celery.task import Task

logger = logging.getLogger(__name__)

DATABASE_NAME = getattr(settings, 'DATABASES')['default']['NAME']
DATABASE_HOST = getattr(settings, 'DATABASES')['default']['HOST']
DATABASE_USER = getattr(settings, 'DATABASES')['default']['USER']
DATABASE_PORT = getattr(settings, 'DATABASES')['default']['PORT']
RUN_DIR = getattr(settings, 'RUN_DIR')
BACKUPS_BACKUP_DIRECTORY = getattr(settings, 'BACKUPS_BACKUP_DIRECTORY')
BACKUPS_BACKUP_CLEANUP_LIMIT = getattr(settings, 'BACKUPS_BACKUP_CLEANUP_LIMIT')
AWS_STORAGE_BUCKET_NAME = getattr(settings, 'AWS_STORAGE_BUCKET_NAME')


class BackupDatabaseTask(Task):
    def run(self):
        management.call_command('backup_postgres')
