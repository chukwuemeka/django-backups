from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging

from django.core.management.base import BaseCommand
from django.utils import timezone
from django.core import management
from storages.backends.s3boto import S3BotoStorage

import os
import gzip
from django.conf import settings
from backups.utils import upload
from backups.utils import listfiles
import logging
from tempfile import NamedTemporaryFile
from fabric.api import local
from fabric.api import settings as fabric_settings


logger = logging.getLogger(__name__)
DATABASE_NAME = getattr(settings, 'DATABASES')['default']['NAME']
DATABASE_HOST = getattr(settings, 'DATABASES')['default']['HOST']
DATABASE_USER = getattr(settings, 'DATABASES')['default']['USER']
DATABASE_PORT = getattr(settings, 'DATABASES')['default']['PORT']
DATABASE_PASSWORD = getattr(settings, 'DATABASES')['default']['PASSWORD']
RUN_DIR = getattr(settings, 'RUN_DIR')
BACKUPS_BACKUP_DIRECTORY = getattr(settings, 'BACKUPS_BACKUP_DIRECTORY')
BACKUPS_BACKUP_CLEANUP_LIMIT = getattr(settings, 'BACKUPS_BACKUP_CLEANUP_LIMIT')
AWS_STORAGE_BUCKET_NAME = getattr(settings, 'AWS_STORAGE_BUCKET_NAME')


class Command(BaseCommand):
    base_options = ()
    option_list = BaseCommand.option_list + base_options

    def add_arguments(self, parser):
        parser.add_argument('-a', '--accesskey', dest='access_key', default=getattr(settings, 'AWS_ACCESS_KEY_ID'))
        parser.add_argument('-s', '--secretkey', dest='secret_key', default=getattr(settings, 'AWS_SECRET_ACCESS_KEY'))
        parser.add_argument('-b', '--bucketname', dest='bucket_name', default=getattr(settings, 'AWS_STORAGE_BUCKET_NAME'))

    def handle(self, *args, **options):
        logger.info('Connecting to S3...')
        s3_boto_storage = S3BotoStorage(
                acl='private',
                access_key=options.get('access_key'),
                secret_key=options.get('secret_key'),
                bucket_name=options.get('bucket_name')
        )

        # Get list of backups
        s3_dir_path = BACKUPS_BACKUP_DIRECTORY + '/%s/' % 'postgres'
        backups = listfiles(s3_boto_storage, s3_dir_path)

        # Download latest backup
        s3_file_path = os.path.join(s3_dir_path, backups[-1])
        file_size_in_kilobytes = s3_boto_storage.size(s3_file_path) / 1024.0
        temp_file = NamedTemporaryFile(delete=False)
        logger.info('Downloading %.2f KB from %s to %s...' % (file_size_in_kilobytes, s3_file_path, temp_file.name))
        backup_file = s3_boto_storage.open(s3_file_path, 'rb')
        temp_file.write(backup_file.read())
        backup_file.close()
        temp_file.close()

        # # Create
        # with fabric_settings(warn_only=True, pgpassword=DATABASE_PASSWORD):
        #     local('dropdb -h %s -U %s -p %s %s' % (
        #         DATABASE_HOST,
        #         DATABASE_USER,
        #         DATABASE_PORT,
        #         DATABASE_NAME,
        #     ))
        #     local('psql -c \"CREATE DATABASE %s;\" -h %s -U %s -p %s' % (
        #         DATABASE_NAME,
        #         DATABASE_HOST,
        #         DATABASE_USER,
        #         DATABASE_PORT,
        #     ))
        #     local('psql -c \"CREATE EXTENSION postgis; CREATE EXTENSION postgis_topology;\" -h %s -U %s -p %s' % (
        #         DATABASE_HOST,
        #         DATABASE_USER,
        #         DATABASE_PORT,
        #     ))
        #     local('psql -c \"ALTER USER %s WITH ENCRYPTED PASSWORD \'%s\';\" -h %s -U %s -p %s' % (
        #         DATABASE_USER,
        #         DATABASE_PASSWORD,
        #         DATABASE_HOST,
        #         DATABASE_USER,
        #         DATABASE_PORT,
        #     ))

        # Restore!
        with fabric_settings(pgpassword=DATABASE_PASSWORD):
            local('pg_restore --clean --dbname=%s --host=%s --port=%s --username=%s %s' % (
                DATABASE_NAME,
                DATABASE_HOST,
                DATABASE_PORT,
                DATABASE_USER,
                temp_file.name
            ))
        os.remove(temp_file.name)
