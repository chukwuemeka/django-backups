from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging

from django.core.management.base import BaseCommand
from django.utils import timezone

import os
from django.conf import settings
from fabric.api import local
from backups.utils import upload


logger = logging.getLogger(__name__)
DATABASE_NAME = getattr(settings, 'DATABASES')['default']['NAME']
DATABASE_HOST = getattr(settings, 'DATABASES')['default']['HOST']
DATABASE_USER = getattr(settings, 'DATABASES')['default']['USER']
DATABASE_PORT = int(getattr(settings, 'DATABASES')['default']['PORT'])
RUN_DIR = getattr(settings, 'RUN_DIR')
VERSION = os.environ.get('VERSION', None)


class Command(BaseCommand):
    base_options = ()
    option_list = BaseCommand.option_list + base_options

    def handle(self, *args, **options):
        logger.info('Running pg_dump...')
        time_text = timezone.now().strftime('%Y-%m-%dT%H:%M:%S%Z')
        if VERSION:
            postgres_file_name = '%s_%s_%s.postgres' % (DATABASE_NAME, VERSION, time_text)
        else:
            postgres_file_name = '%s_%s.postgres' % (DATABASE_NAME, time_text)
        postgres_file_path = os.path.join(RUN_DIR, postgres_file_name)
        local('pg_dump -F c -d %s -h %s -U %s -p %d > %s' % (DATABASE_NAME, DATABASE_HOST, DATABASE_USER, DATABASE_PORT, postgres_file_path))
        upload(postgres_file_path, 'postgres')
        os.remove(postgres_file_path)
